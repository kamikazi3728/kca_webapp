import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


import './App.css';
import HeaderBar from './HeaderBar';
import BodyContainer from './Body'
import Footer from './Footer'

const navBarOptions = ["Home","How it works","Next Steps","Contact"];

class App extends React.Component {
	constructor(props){
		super(props);this.state={currentPage:0};
		this.changePage=this.changePage.bind(this);
		this.returnFunc=this.returnFunc.bind(this);
	}
	returnFunc(fxn){this.returnFunc=fxn;};
	changePage(changeTo){this.setState({currentPage:changeTo});this.returnFunc(changeTo);};
	render(){return (
		<><HeaderBar bodyChangeFunc={this.changePage} navOpts={navBarOptions}/>
		<BodyContainer register={this.returnFunc} page={this.state.currentPage}/>
		<Footer /></>
	);}
}

ReactDOM.render(<React.StrictMode><App /></React.StrictMode>,document.getElementById('App'));

// If you want to start measuring performance in your app, pass a function			---------------------------------------
// to log results (for example: reportWebVitals(console.log))						----------------------------------------------------
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals			---------------------------------------

/*
import reportWebVitals from './reportWebVitals';
reportWebVitals();
*/