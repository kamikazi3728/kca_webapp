import React from 'react';

export default class BodyContainer extends React.Component {
	constructor(props){
		super(props);
		this.state={page:this.props.page};
		this.changePage=this.changePage.bind(this);
		this.props.register(this.changePage);
	}
	changePage(pg){this.setState({page:pg});};
	render(){
				//-----------------------------------------------------------------------------------------------------------------
		switch(this.state.page){
			case 1: //case for 1, How it works
				return(<div className="BodyContainer"><div className="BodyContent">
							<h1>How it works</h1>
							<div className="splitVert">
								<div className="splitHoriz"><span><h2 className="tableTop">Conventional Testing</h2></span><span><h2 className="tableTop">Well Aware App</h2></span></div>
								<div className="splitHoriz"><span>{"Historically, the only testing option"}</span><span>A new testing option</span></div>
								<div className="splitHoriz"><span>{"Slow results (days to weeks)"}</span><span>Results in ~1 day</span></div>
								<div className="splitHoriz"><span>Costly: commercial and state laboratory testing costs often ~$100</span><span>Cost effective (~$20-25 per kit)</span></div>
								<div className="splitHoriz"><span>Results difficult to interpret</span><span>Provides links to information on safety standards, treatment options, and lab testing</span></div>
								<div className="splitHoriz"><span>Follow-up action may not occur</span><span>Recommends follow-up lab testing for exceedances or ambguous results</span></div>
								<div className="splitHoriz"><span></span><span>Can include reminders to test at recommended intervals</span></div>
							</div>
							<div><h2>E. coli testing</h2><ul>
								<li><p>Compartment Bag Test - well-validated but challenging to interpret</p></li>
								<li><p>1 day of ambient incubation</p></li>
								<li><p>Upload image to Well Aware</p></li>
								<li><p>E. coli detection by novel machine learning and image recognition of color changes</p></li>
							</ul></div>
							<div><h2>Lead testing</h2><ul>
								<li><p>Lead test strip - robust detection but challenging to interpret</p></li>
								<li><p>10 minutes in sample</p></li>
								<li><p>Upload image to Well Aware</p></li>
								<li><p>Lead detection by novel image recognition of color changing lines on test strip</p></li>
							</ul></div>
							<div><h2>If your tests are positive for E. coli or lead contamination</h2><ul>
								<li><p><h2>the Well Aware app will let you know:</h2></p></li>
								<li><p>{"The estimated level of contamination and associated risk category (low, intermediate, high)"}</p></li>
								<li><p>Who to contact about the contamination</p></li>
								<li><p>How to obtain follow-up lab testing</p></li>
							</ul></div>
				</div></div>);
				//-----------------------------------------------------------------------------------------------------------------
			case 2: //case for 2, Next steps
				return(<div className="BodyContainer"><div className="BodyContent">
							<h1>What's Next?</h1>
							<div><h2>How Well Aware can collaborate with Health Departments</h2><ul>
								<li><p>Connect users with available resources
								</p></li><li><p>Serve as an “on-ramp” to regular testing, well safety information
								</p></li><li><p>More frequent well testing will provide data about overall well water quality in North Carolina
								</p></li><li><p>Use of Well Aware can increase testing to reduce well population drinking unsafe water due to microbial and lead contamination
								</p></li><li><p>Improve the health of well water users across North Carolina
								</p></li><li><p>Design app to connect users to resources while limiting extraneous calls to the Department of Public Health about well testing
							</p></li></ul></div>
							<div><h2>Further Testing</h2><ul>
								<li><p>Piloting entire test kit at local wells
								</p></li><li><p>Ease of use for App
								</p></li><li><p>Marketability and relevance to community
								</p></li><li><p>Providing data to the Water Institute and DPH on well water quality
							</p></li></ul></div>
				</div></div>);
				//-----------------------------------------------------------------------------------------------------------------
			case 3: //case for 3, Contact
				return(<div className="BodyContainer"><div className="BodyContent">
							<div><h1>Contact</h1></div>
							<div><h3>Main Contact: Michael Fisher - mischaf2@email.unc.edu</h3></div>
				</div></div>);
				//-----------------------------------------------------------------------------------------------------------------
			default:	//case for 0, default, About wellaware and why it is needed
				return(<div className="BodyContainer"><div className="BodyContent">
							<div><h1>Well Aware</h1><ul>
								<li><p>Free, open-source mobile app</p></li>
								<li><p>Low-cost validated home test kit</p></li>
								<li><p>Link to local resources and environmental health professionals at local health departments</p></li>
							</ul></div>
							<div><h1>Why Well Aware?</h1><ul>
								<li><p>More than 43m people in US rely on private wells- more than 3 million in NC</p></li>
								<li><p>Most do not test well water quality on a regular basis</p></li>
								<li><p>NC DPH recommends tests for microbial contamination yearly and lead every 2 years</p></li>
								<li><p>This rarely occurs in practice</p></li>
							</ul></div>
							<div><h1>Testing Background: Microbes</h1><ul>
								<li><p>Drinking water is an important source of exposure to pathogens</p></li>
								<li><p>Gastrointestinal illnesses associated with drinking-water consumption range from 12 to 19x10<sup>6</sup> cases/year in the US</p></li>
								<li><p>Wells not regulated by the Safe Drinking Water Act</p></li>
								<li><p>Private groundwater wells particularly at risk</p></li>
								<li><p>Most wells never tested after installation</p></li>
							</ul></div>
							<div><h1>Testing Background: Lead</h1><ul>
								<li><p>Neurotoxic – harms development</p></li>
								<li><p>Drinking water is an important source</p></li>
								<li><p>No safe level, but lower is better</p></li>
								<li><p>Children in Wake County had blood lead levels from 1-35 µg/dL</p></li>
								<li><p>{"Children on private wells 25% (p<0.05) more likely to have elevated blood lead"}</p></li>
							</ul></div>
							<div>
								<h2>Special thanks to our app developer <a href="https://www.crosscomm.com">CrossComm</a></h2>
								<div><a href="https://www.crosscomm.com"><img src="crosscomm.png" alt="CrossComm Logo" id="cclogo"></img></a></div>
							</div>
							<div>
								
							</div>
							
				</div></div>);
		}
	}
}