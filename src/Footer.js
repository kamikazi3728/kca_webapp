import React from 'react';

export default class Footer extends React.Component {
	render(){return(
		<div className="FooterContainer topOrBot"><div className="Footer">
			<p className="FooterContent">
			{/*-----------------------------------------------------------------------------------------------*/}
			</p>
			<p className="FooterContent">Copyright &#169; 2021 Well Aware. All Rights Reserved.</p>
			<p className="FooterContent">Built with <a href="https://reactjs.org/" className="Footerlink">React</a> by: Reece Appling</p>
		</div></div>
	);}
}